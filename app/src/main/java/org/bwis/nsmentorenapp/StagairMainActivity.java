package org.bwis.nsmentorenapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by mediacenter on 26-2-2016.
 */
public class StagairMainActivity extends AppCompatActivity {
    private static final String PREFS = "prefs";
    private static final String PREF_NAME = "name";
    private static final String PREF_PNR = "id";
    private static final String PREF_ID = "id";
    private static final String PREF_FUNCTIE = "functie";
    private static final String PREF_VOORNAAM = "voornaam";
    private static final String PREF_TUSSENVOEGSEL = "tussenvoegsel";
    private static final String PREF_ACHTERNAAM = "achternaam";
    private static final String PREF_NEWINSTALL = "newInstall";
    private static final String PREF_AANTALGROEPEN = "aantalGroepen";
    private static final String PREF_MENTORENGROEP = "mentorenGroep";

    SharedPreferences mSharedPreferences;
    TextView displayID;
    Button leerlingen_lijst, mentoren_lijst, leerlingen_prikbord, stagairs_lijst, mail_button;

    methods method = new methods();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stagair_activity_main);


        displayID = (TextView) findViewById(R.id.displayID);
        stagairs_lijst = (Button)findViewById(R.id.stagairs_lijst);
        leerlingen_lijst = (Button)findViewById(R.id.leerlingen_lijst);
        mentoren_lijst = (Button)findViewById(R.id.mentoren_lijst);
        leerlingen_prikbord = (Button)findViewById(R.id.leerlingen_prikbord);
        mail_button = (Button)findViewById(R.id.mail_button);


        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        String id = mSharedPreferences.getString(PREF_ID, "");
        String functie = mSharedPreferences.getString(PREF_FUNCTIE, "");
        String newInstall = mSharedPreferences.getString(PREF_NEWINSTALL, "");
        String voornaam = mSharedPreferences.getString(PREF_VOORNAAM, "");
        String tussenvoegsel = mSharedPreferences.getString(PREF_TUSSENVOEGSEL, "");
        String achternaam = mSharedPreferences.getString(PREF_ACHTERNAAM, "");
        final String mentorenGroep = mSharedPreferences.getString(PREF_MENTORENGROEP, "?");
        String volNaam;


        String voledigeNaam = method.volnaam_method(voornaam, achternaam, tussenvoegsel);
        String functieNaam = method.functienaam_method(functie);

        final String versionName = BuildConfig.VERSION_NAME;
        if(!newInstall.equals(versionName)){

            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(PREF_NEWINSTALL, versionName);
            e.commit();

            AlertDialog.Builder builder = new AlertDialog.Builder(StagairMainActivity.this);

            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.whatsnew_popup,null);

            builder.setView(dialogView);

            final TextView popup_text = (TextView) dialogView.findViewById(R.id.text_pop);
            final Button closeBtn = (Button)dialogView.findViewById(R.id.button_popup_close);

            final AlertDialog dialog = builder.create();

            closeBtn .setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();


        }


        displayID.setText(functieNaam + ": " + voledigeNaam + " - " + mentorenGroep);

        leerlingen_prikbord.setEnabled(false);
        leerlingen_prikbord.setAlpha(.0f);

        stagairs_lijst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), UsersActivity.class);
                intent.putExtra("UserType", "stagair");
                startActivity(intent);
            }
        });

        leerlingen_lijst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), UsersActivity.class);
                intent.putExtra("UserType", "leerling");
                startActivity(intent);
            }
        });

        mentoren_lijst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(),UsersActivity.class);
                intent.putExtra("UserType", "mentor");
                startActivity(intent);
            }
        });

        leerlingen_prikbord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), UsersActivity.class);
                intent.putExtra("UserType", "leerlingen_prikbord");
                startActivity(intent);
            }
        });

        mail_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + "martijn.talens@ns.nl;"));
                emailIntent.putExtra(Intent.EXTRA_CC, "martijn.talens@gmail.com");
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NSmentorenApp - contact");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Ik heb de volgende vraag/opmerking over de NSmentorenApp:");

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send email using..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(StagairMainActivity.this, "No email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu){

        MenuItem groep = menu.findItem(R.id.action_groepen_wijzigen);

        final String aantalGroepen = mSharedPreferences.getString(PREF_AANTALGROEPEN, "1");

        int aantal_groepen = Integer.parseInt(aantalGroepen);
        if(aantal_groepen > 1){
            groep.setVisible(true);
        }else {
            groep.setVisible(false);
        }
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            final String versionName = BuildConfig.VERSION_NAME;
            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
            SharedPreferences.Editor e = mSharedPreferences.edit().clear();
            e.putString(PREF_NEWINSTALL, versionName);
            e.commit();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.action_exit) {

            this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        if (id == R.id.action_groepen_wijzigen) {

            System.out.println("mentorengroep: "+ "input");

            Intent intent = new Intent(getApplicationContext(), MentorenGroepActivity.class);
            intent.putExtra("UserType", "leerling");
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }
}
