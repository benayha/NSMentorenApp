package org.bwis.nsmentorenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mediacenter on 28-2-2016.
 */

public class UsersActivity extends AppCompatActivity {

    TextView displayID;
    ListView listUsers;
    ArrayList<String> UsersList;
    RequestQueue requestQueue;
    Button heading;
    ArrayList<String> your_array_list = new ArrayList<String>();

    private static final String PREFS = "prefs";
    private static final String PREF_NAME = "name";
    private static final String PREF_PNR = "id";
    private static final String PREF_ID = "id";
    private static final String PREF_FUNCTIE = "functie";
    private static final String PREF_VOORNAAM = "voornaam";
    private static final String PREF_TUSSENVOEGSEL = "tussenvoegsel";
    private static final String PREF_ACHTERNAAM = "achternaam";
    private static final String PREF_NEWINSTALL = "newInstall";
    private static final String PREF_MENTORENGOEP = "mentorenGroep";
    private static final String PREF_AANTALGROEPEN = "aantalGroepen";

    SharedPreferences mSharedPreferences;

    methods method = new methods();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_activity_main);

        Intent intent = getIntent();
        final String userType = intent.getStringExtra("UserType");
        String headerText= "";


        final ListView lv1 = (ListView) findViewById(R.id.listUsers);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String out = your_array_list.get(position);

                String[] personeelsnummer_split = out.split(":p;");
                String personeelsNummer = personeelsnummer_split[1];

                //Toast.makeText(UsersActivity.this, "Selected :" + personeelsNummer , Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getApplicationContext(),LeerlingenViewActivity.class);
                intent.putExtra("personeelsnummer", personeelsNummer);
                startActivity(intent);


            }
        });

        displayID = (TextView) findViewById(R.id.displayID);
        //listUsers = (ListView) findViewById(R.id.listUsers);
        String GetUsersUrl = "https://nspersoneel.nl/mentorenApp/GetUsers.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        if(userType.equals("leerling")){
            headerText = "Leerlingen";
        }
        if(userType.equals("mentor")){
            headerText = "mentoren";
        }
        if(userType.equals("Team Manager")){
            headerText = "Team Managers";
        }
        //heading.setText(headerText);

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        String id = mSharedPreferences.getString(PREF_ID, "");
        String functie = mSharedPreferences.getString(PREF_FUNCTIE, "");
        String voornaam = mSharedPreferences.getString(PREF_VOORNAAM, "");
        String tussenvoegsel = mSharedPreferences.getString(PREF_TUSSENVOEGSEL, "");
        String achternaam = mSharedPreferences.getString(PREF_ACHTERNAAM, "");
        String volNaam;
        final String mentorenGroep = mSharedPreferences.getString(PREF_MENTORENGOEP, "?");

        String voledigeNaam = method.volnaam_method(voornaam, achternaam, tussenvoegsel);
        String functieNaam = method.functienaam_method(functie);


        displayID.setText(functieNaam + ": " + voledigeNaam + " - " + mentorenGroep);

        HttpsTrustManager.allowAllSSL();
        StringRequest request = new StringRequest(Request.Method.POST,GetUsersUrl, new Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                try {
                    JSONObject jsnobject = new JSONObject(response);

                    JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");

                    System.out.println("mentorengroep: "+ mentorenGroep);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject statusObject = jsonArray.getJSONObject(i);

                        String extractUsersAchternaam = statusObject.getString("achternaam");
                        String extractUsersVoornaam = statusObject.getString("voornaam");
                        String extractUsersTussenvoegsel = statusObject.getString("tussenvoegsel");
                        String extractUsersTelefoon = statusObject.getString("telefoon");
                        String extractUsersPersoneelsnummer = statusObject.getString("persooneelsnummer");

                        String extractUsers;

                        if(extractUsersTussenvoegsel.equals("NULL")){
                            extractUsers = extractUsersVoornaam + " " + extractUsersAchternaam + ":t;" + extractUsersTelefoon + ":p;" + extractUsersPersoneelsnummer;
                        }else{
                            extractUsers = extractUsersVoornaam + " " + extractUsersTussenvoegsel + " " + extractUsersAchternaam + ":t;" + extractUsersTelefoon + ":p;" + extractUsersPersoneelsnummer;
                        }


                        your_array_list.add(extractUsers);

                    }

                    //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UsersActivity.this, android.R.layout.simple_list_item_1, your_array_list);
                    //listUsers.setAdapter(arrayAdapter);
                    GetArrayValue();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters = new HashMap<String, String>();

                parameters.put("usertype", (userType));
                parameters.put("mentoren_groep", (mentorenGroep));
                return parameters;

            }
        };
        requestQueue.add(request);






    }

    private void GetArrayValue(){

        ArrayList image_details = getListData();
        final ListView lv1 = (ListView) findViewById(R.id.listUsers);
        lv1.setAdapter(new CustomListAdapter(this, image_details));
    }

    private ArrayList getListData() {


        ArrayList<NewsItem> results = new ArrayList<NewsItem>();

        for (int i = 0; i <your_array_list.size(); i++) {


            NewsItem newsData = new NewsItem();
            String[] separated = your_array_list.get(i).split(":");

            newsData.setHeadline(separated[0]);

            String telefoonNr = separated[1];
            String[] tFoon = telefoonNr.split(";");
            telefoonNr = tFoon[1];


            if(telefoonNr.equals("NULL")) {
                newsData.setHeadline(separated[0]);
                newsData.setReporterName("");

            }else{
                newsData.setHeadline(separated[0]);
                newsData.setReporterName("tel: " + telefoonNr);
            }

            //displayID.setText(separated[1]);
            //newsData.setDate("May 26, 2013, 13:35");
            results.add(newsData);
        }

        // Add some more dummy data for testing
        return results;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){

        MenuItem groep = menu.findItem(R.id.action_groepen_wijzigen);

        final String aantalGroepen = mSharedPreferences.getString(PREF_AANTALGROEPEN, "1");

        int aantal_groepen = Integer.parseInt(aantalGroepen);
        if(aantal_groepen > 1){
            groep.setVisible(true);
        }else {
            groep.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            final String versionName = BuildConfig.VERSION_NAME;
            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
            SharedPreferences.Editor e = mSharedPreferences.edit().clear();
            e.putString(PREF_NEWINSTALL, versionName);
            e.commit();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.action_exit) {

            this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        if (id == R.id.action_groepen_wijzigen) {

            System.out.println("mentorengroep: "+ "input");

            Intent intent = new Intent(getApplicationContext(), MentorenGroepActivity.class);
            intent.putExtra("UserType", "leerling");
            startActivity(intent);
            this.finish();

        }
        return super.onOptionsItemSelected(item);
    }


}
