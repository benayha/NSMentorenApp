package org.bwis.nsmentorenapp;

/**
 * Created by mediacenter on 2-3-2016.
 */
public class NewsItem {
    private String headline;
    private String reporterName;
    private String date;
    private String beoordeling;
    private String pic;

    public String getHeadline() {

        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getReporterName() {
        return reporterName;
    }

    public void setBeoordeling(String beoordeling) {
        this.reporterName = beoordeling;
    }

    public String getBeoordeling() {
        return beoordeling;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPic() {
        return pic;
    }
}