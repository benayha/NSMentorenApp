package org.bwis.nsmentorenapp;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.net.sip.SipAudioCall;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mediacenter on 22-3-2016.
 */



public class LeerlingenViewActivity extends AppCompatActivity {
    private static final String PREFS = "prefs";
    private static final String PREF_NAME = "name";
    private static final String PREF_PNR = "id";
    private static final String PREF_ID = "id";
    private static final String PREF_FUNCTIE = "functie";
    private static final String PREF_VOORNAAM = "voornaam";
    private static final String PREF_TUSSENVOEGSEL = "tussenvoegsel";
    private static final String PREF_ACHTERNAAM = "achternaam";
    private static final String PREF_NEWINSTALL = "newInstall";
    private static final String PREF_AANTALGROEPEN = "aantalGroepen";
    private static final String PREF_MENTORENGROEP = "mentorenGroep";

    private String pNummer, blokaInput, blokbInput, blokcInput;

    SharedPreferences mSharedPreferences;
    TextView tViewPnummer;
    TextView tViewNaam;
    TextView tViewNaam_extra;
    TextView tViewFunctie;
    TextView tViewStandplaats;
    TextView tViewTelefoon;
    TextView tViewInDienstSinds;
    TextView tViewBlokA;
    TextView tViewBlokC;
    TextView tViewBlokB;
    LinearLayout layoutT1, layoutT2, layoutT3, layoutT4, layoutT5, layoutT6, layoutT7, layoutT8, layoutBeoordeling, layoutExtraInfo, layout_buttons_opleiding;


    TextView displayID, hideButton, hideButton2;
    RequestQueue requestQueue;
    ArrayList<String> your_array_list = new ArrayList<String>();
    ImageView beoordeling_1, beoordeling_2, beoordeling_3, beoordeling_4, beoordeling_5, beoordeling_6, beoordeling_7, beoordeling_8;
    Button button_extra;

    methods method = new methods();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.leerlingen_view_activity_main);

        displayID = (TextView) findViewById(R.id.displayID);
        hideButton= (TextView)findViewById(R.id.hide_button);
        hideButton2= (TextView)findViewById(R.id.hide_button2);

        beoordeling_1 = (ImageView) findViewById(R.id.imageMat);
        beoordeling_2 = (ImageView) findViewById(R.id.imageSeingeving);
        beoordeling_3 = (ImageView) findViewById(R.id.T3);
        beoordeling_4 = (ImageView) findViewById(R.id.imageRangeerproces);
        beoordeling_5 = (ImageView) findViewById(R.id.imageConcentratie);
        beoordeling_6 = (ImageView) findViewById(R.id.imageInuitdienst);
        beoordeling_7 = (ImageView) findViewById(R.id.imageKleding);
        beoordeling_8 = (ImageView) findViewById(R.id.imageHouding);

        layoutT1 = (LinearLayout) findViewById(R.id.layoutT1);
        layoutT2 = (LinearLayout) findViewById(R.id.layoutT2);
        layoutT3 = (LinearLayout) findViewById(R.id.layoutT3);
        layoutT4 = (LinearLayout) findViewById(R.id.layoutT4);
        layoutT5 = (LinearLayout) findViewById(R.id.layoutT5);
        layoutT6 = (LinearLayout) findViewById(R.id.layoutT6);
        layoutT7 = (LinearLayout) findViewById(R.id.layoutT7);
        layoutT8 = (LinearLayout) findViewById(R.id.layoutT8);
        layoutBeoordeling = (LinearLayout) findViewById(R.id.layoutBeoordelingen);
        layoutExtraInfo = (LinearLayout) findViewById(R.id.layoutExtraInfo);
        layout_buttons_opleiding = (LinearLayout)findViewById(R.id.layout_buttons_opleiding);

        String UserId;

        Intent intent = getIntent();
        pNummer = intent.getStringExtra("personeelsnummer");

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        String id = mSharedPreferences.getString(PREF_ID, "");
        final String functie = mSharedPreferences.getString(PREF_FUNCTIE, "");
        String voornaam = mSharedPreferences.getString(PREF_VOORNAAM, "");
        String tussenvoegsel = mSharedPreferences.getString(PREF_TUSSENVOEGSEL, "");
        String achternaam = mSharedPreferences.getString(PREF_ACHTERNAAM, "");
        final String mentorenGroep = mSharedPreferences.getString(PREF_MENTORENGROEP, "?");
        String volNaam;

        Button button_info =(Button)findViewById(R.id.button_info_down);
        Button button_extra =(Button)findViewById(R.id.button_extra_down);

        button_extra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                layoutExtraInfo.setVisibility(layoutExtraInfo.VISIBLE);
                layout_buttons_opleiding.setVisibility(layout_buttons_opleiding.VISIBLE);
                hideButton2.setVisibility(hideButton2.VISIBLE);

            }
        });


        button_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                layoutExtraInfo.setVisibility(layoutExtraInfo.GONE);
                layout_buttons_opleiding.setVisibility(layout_buttons_opleiding.GONE);
                hideButton2.setVisibility(hideButton2.GONE);

            }
        });

        String voledigeNaam = method.volnaam_method(voornaam, achternaam, tussenvoegsel);
        String functieNaam = method.functienaam_method(functie);

        displayID.setText(functieNaam + ": " + voledigeNaam + " - " + mentorenGroep);

        getDBdata();

        ImageView bindImage = (ImageView)findViewById(R.id.imageView2);
        ImageView bindImage2 = (ImageView)findViewById(R.id.imageView3);

        String pathToFile = "https://nspersoneel.nl/mentorenApp/foto/" + pNummer + ".jpg";

        DownloadImageWithURLTask downloadTask = new DownloadImageWithURLTask(bindImage);
        downloadTask.execute(pathToFile);

        DownloadImageWithURLTask downloadTask2 = new DownloadImageWithURLTask(bindImage2);
        downloadTask2.execute(pathToFile);

        layoutT1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingMainActivity.class);
                intent.putExtra("personeelnummer", pNummer);
                intent.putExtra("beoordeling", "1");
                startActivity(intent);
            }
        });
        layoutT2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingMainActivity.class);
                intent.putExtra("personeelnummer", pNummer);
                intent.putExtra("beoordeling", "2");
                startActivity(intent);
            }
        });
        layoutT3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingMainActivity.class);
                intent.putExtra("personeelnummer", pNummer);
                intent.putExtra("beoordeling", "3");
                startActivity(intent);

            }
        });
        layoutT4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingMainActivity.class);
                intent.putExtra("personeelnummer", pNummer);
                intent.putExtra("beoordeling", "4");
                startActivity(intent);
            }
        });
        layoutT5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingMainActivity.class);
                intent.putExtra("personeelnummer", pNummer);
                intent.putExtra("beoordeling", "5");
                startActivity(intent);
            }
        });
        layoutT6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingMainActivity.class);
                intent.putExtra("personeelnummer", pNummer);
                intent.putExtra("beoordeling", "6");
                startActivity(intent);
            }
        });
        layoutT7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingMainActivity.class);
                intent.putExtra("personeelnummer", pNummer);
                intent.putExtra("beoordeling", "7");
                startActivity(intent);
            }
        });
        layoutT8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingMainActivity.class);
                intent.putExtra("personeelnummer", pNummer);
                intent.putExtra("beoordeling", "8");
                startActivity(intent);
            }
        });
    }

    private class DownloadImageWithURLTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageWithURLTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String pathToFile = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(pathToFile).openStream();
                bitmap = BitmapFactory.decodeStream(in);
                System.out.println("path to file: "  + pathToFile);
            } catch (Exception e) {
                Log.e("Error 77", e.getMessage());
                e.printStackTrace();
            }
            return bitmap;
        }
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        getDBdata();
    }

    public void getDBdata() {
        String GetUserDataUrl = "https://nspersoneel.nl/mentorenApp/GetUserData.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        HttpsTrustManager.allowAllSSL();
        StringRequest request = new StringRequest(Request.Method.POST,GetUserDataUrl, new Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                try {
                    JSONObject jsnobject = new JSONObject(response);

                    JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");



                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject statusObject = jsonArray.getJSONObject(i);

                        String extractUsersAchternaam = statusObject.getString("achternaam");
                        String extractUsersVoornaam = statusObject.getString("voornaam");
                        String extractUsersTussenvoegsel = statusObject.getString("tussenvoegsel");
                        final String extractUsersTelefoon = statusObject.getString("telefoon");
                        String extractUsersPersoneelsnummer = statusObject.getString("persooneelsnummer");
                        String extractUsersFunctie = statusObject.getString("functie");
                        String extractUsersStandplaats = statusObject.getString("standplaats");
                        String extractUsersStageJaar = statusObject.getString("stage_jaar");
                        String extractUsersInDienstSinds = statusObject.getString("in_dienst_sinds");
                        String extractUsersBlokaGepland = statusObject.getString("bloka_gepland");
                        String extractUsersBlokaResultaat = statusObject.getString("bloka_resultaat");
                        String extractUsersBlokbGepland = statusObject.getString("blokb_gepland");
                        String extractUsersBlokbResultaat = statusObject.getString("blokb_resultaat");
                        String extractUsersBlokcGepland = statusObject.getString("blokc_gepland");
                        String extractUsersBlokcResultaat = statusObject.getString("blokc_resultaat");
                        String extractUsersT1 = statusObject.getString("T1");
                        String extractUsersT2 = statusObject.getString("T2");
                        String extractUsersT3 = statusObject.getString("T3");
                        String extractUsersT4 = statusObject.getString("T4");
                        String extractUsersT5 = statusObject.getString("T5");
                        String extractUsersT6 = statusObject.getString("T6");
                        String extractUsersT7 = statusObject.getString("T7");
                        String extractUsersT8 = statusObject.getString("T8");
                        String extractUsersT9 = statusObject.getString("T9");
                        String extractUsersT10 = statusObject.getString("T10");
                        String extractUsersT11 = statusObject.getString("T11");
                        String extractUsersT12 = statusObject.getString("T12");
                        String extractUsersT13 = statusObject.getString("T13");
                        String extractUsersT14 = statusObject.getString("T14");
                        String extractUsersT15 = statusObject.getString("T15");
                        //UserId = statusObject.getString("id");

                        String extractUsers;
                        if(extractUsersFunctie.equals("Leerling")){

                            Button button_info =(Button)findViewById(R.id.button_info);
                            button_info.setVisibility(button_info.VISIBLE);
                            Button button_extra =(Button)findViewById(R.id.button_extra_down);
                            button_extra.setVisibility(button_extra.GONE);

                            hideButton.setVisibility(hideButton.VISIBLE);

                            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
                            final String gebruikersFunctie = mSharedPreferences.getString(PREF_FUNCTIE, "");
                            String id = mSharedPreferences.getString(PREF_ID, "");

                            if(gebruikersFunctie.equals("Mentor")) {

                                layoutBeoordeling.setVisibility(layoutBeoordeling.VISIBLE);
                                button_extra.setVisibility(button_extra.VISIBLE);
                            }
                            if(gebruikersFunctie.equals("Team Manager")) {

                                layoutBeoordeling.setVisibility(layoutBeoordeling.VISIBLE);
                                button_extra.setVisibility(button_extra.VISIBLE);

                            }
                            if(gebruikersFunctie.equals("Leerling")){
                                if(extractUsersPersoneelsnummer.equals(id)) {
                                    layoutBeoordeling.setVisibility(layoutBeoordeling.VISIBLE);
                                    button_extra.setVisibility(button_extra.VISIBLE);
                                }
                            }
                        }

                        if(extractUsersFunctie.equals("Stagair")){
                            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
                            final String gebruikersFunctie = mSharedPreferences.getString(PREF_FUNCTIE, "");
                            String id = mSharedPreferences.getString(PREF_ID, "");

                            Button button_extra =(Button)findViewById(R.id.button_extra_down);
                            button_extra.setVisibility(button_extra.GONE);
                            Button button_info =(Button)findViewById(R.id.button_info);
                            button_info.setVisibility(button_extra.VISIBLE);

                            hideButton.setVisibility(hideButton.VISIBLE);

                            if(gebruikersFunctie.equals("Mentor")) {

                                layoutBeoordeling.setVisibility(layoutBeoordeling.VISIBLE);
                                button_extra.setVisibility(button_extra.VISIBLE);
                            }
                            if(gebruikersFunctie.equals("Team Manager")) {

                                layoutBeoordeling.setVisibility(layoutBeoordeling.VISIBLE);
                                button_extra.setVisibility(button_extra.VISIBLE);
                            }
                            if(gebruikersFunctie.equals("Stagair")){
                                if(extractUsersPersoneelsnummer.equals(id)) {
                                    layoutBeoordeling.setVisibility(layoutBeoordeling.VISIBLE);
                                    button_extra.setVisibility(button_extra.VISIBLE);
                                }
                            }
                        }

                        if(extractUsersTussenvoegsel.equals("NULL")){
                            extractUsers = extractUsersVoornaam + " " + extractUsersAchternaam;
                        }else{
                            extractUsers = extractUsersVoornaam + " " + extractUsersTussenvoegsel + " " + extractUsersAchternaam;
                        }

                        blokaInput = "Blok A: <b>" + extractUsersBlokaGepland + "</b>";

                        if(extractUsersBlokaGepland.equals("null")){
                            blokaInput = "Blok A: <b>";
                        }
                        if(extractUsersBlokaResultaat.substring(0,1).equals("V")){
                            blokaInput = "Blok A: <font color=#008000><b>" + extractUsersBlokaGepland + "</b></font>";
                        }

                        if(extractUsersBlokaResultaat.substring(0,1).equals("O")){
                            blokaInput = "Blok A: <font color=#FF0000><b>" + extractUsersBlokaGepland + "</b></font>";
                        }

                        blokbInput = "Blok B: <b>" + extractUsersBlokbGepland + "</b>";

                        if(extractUsersBlokbGepland.equals("null")){
                            blokbInput = "Blok B: <b>";
                        }
                        if(extractUsersBlokbResultaat.substring(0,1).equals("V")){
                            blokbInput = "Blok B: <font color=#008000><b>" + extractUsersBlokbGepland + "</b></font>";
                        }

                        if(extractUsersBlokbResultaat.substring(0,1).equals("O")){
                            blokbInput = "Blok B: <font color=#FF0000><b>" + extractUsersBlokbGepland + "</b></font>";
                        }

                        blokcInput = "Blok C: <b>" + extractUsersBlokcGepland + "</b>";

                        if(extractUsersBlokcGepland.equals("null")){
                            blokcInput = "Blok C: <b>";
                        }
                        if(extractUsersBlokcResultaat.substring(0,1).equals("V")){
                            blokcInput = "Blok C: <font color=#008000><b>" + extractUsersBlokcGepland + "</b></font>";
                        }

                        if(extractUsersBlokcResultaat.substring(0,1).equals("O")){
                            //if(extractUsersBlokaGepland.equals("null")) {
                            String OnvoldoendeDatum = extractUsersBlokcResultaat.substring(12,22);
                            blokcInput = "Blok C: <b><font color=#FF0000" + OnvoldoendeDatum +
                                    "</b></font>";
                            //}
                        }

                        tViewNaam = (TextView) findViewById(R.id.textViewNaam);
                        tViewNaam.setText(Html.fromHtml("Naam: <b>" + extractUsers + "</b>"));

                        tViewNaam_extra = (TextView) findViewById(R.id.textViewNaam2);
                        tViewNaam_extra.setText(Html.fromHtml("Naam: <b>" + extractUsers + "</b>"));

                        tViewPnummer = (TextView) findViewById(R.id.textViewPnummer);
                        tViewPnummer.setText(Html.fromHtml("Personeelsnummer: <b>" + extractUsersPersoneelsnummer + "</b>"));

                        tViewInDienstSinds = (TextView)findViewById(R.id.textViewInDienst);
                        tViewInDienstSinds.setText(Html.fromHtml("In dienst sinds: <b>" + extractUsersInDienstSinds+ "</b>"));

                        tViewBlokA = (TextView)findViewById(R.id.textViewBlokA);
                        tViewBlokA.setText(Html.fromHtml(blokaInput));

                        tViewBlokB = (TextView)findViewById(R.id.textViewBlokB);
                        tViewBlokB.setText(Html.fromHtml(blokbInput));

                        tViewBlokC = (TextView)findViewById(R.id.textViewBlokC);
                        tViewBlokC.setText(Html.fromHtml(blokcInput));

                        if(extractUsersFunctie.equals("Stagair")) {
                            tViewFunctie = (TextView) findViewById(R.id.textViewFunctie);
                            String functieNaamCheck = method.functienaam_method(extractUsersFunctie);

                            functieNaamCheck = functieNaamCheck + " (" + extractUsersStageJaar + "e jaars)";

                            tViewFunctie.setText(Html.fromHtml("Functie: <b>" + functieNaamCheck + "</b>"));
                        }else{
                            tViewFunctie = (TextView) findViewById(R.id.textViewFunctie);
                            String functieNaamCheck = method.functienaam_method(extractUsersFunctie);
                            tViewFunctie.setText(Html.fromHtml("Functie: <b>" + functieNaamCheck + "</b>"));
                        }

                        tViewStandplaats = (TextView) findViewById(R.id.textViewStandplaats);
                        tViewStandplaats.setText(Html.fromHtml("Standplaats: <b>" + extractUsersStandplaats + "</b>"));

                        tViewTelefoon = (TextView) findViewById(R.id.textViewTelefoon);
                        tViewTelefoon.setText(Html.fromHtml("Telefoon: <b><u>" + extractUsersTelefoon + "</u></b>"));

                        tViewTelefoon.setOnClickListener(new View.OnClickListener() {

                            public void onClick(View v) {
                                try {
                                    startActivity( new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + extractUsersTelefoon)));
                                } catch (ActivityNotFoundException activityException) {
                                    Log.e("Calling a Phone Number", "Call failed", activityException);
                                }
                            }
                        });

                        if(extractUsersT1.equals("3")) {
                            beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                        }
                        if(extractUsersT1.equals("2")) {
                            beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                        }
                        if(extractUsersT1.equals("1")) {
                            beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                        }
                        if(extractUsersT1.equals("0")) {
                            beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                        }

                        if(extractUsersT2.equals("3")) {
                            beoordeling_2.setImageResource(R.drawable.sein_groen_s);
                        }
                        if(extractUsersT2.equals("2")) {
                            beoordeling_2.setImageResource(R.drawable.sein_geel_s);
                        }
                        if(extractUsersT2.equals("1")) {
                            beoordeling_2.setImageResource(R.drawable.sein_rood_s);
                        }
                        if(extractUsersT2.equals("0")) {
                            beoordeling_2.setImageResource(R.drawable.sein_geel_s);
                        }

                        if(extractUsersT3.equals("3")) {
                            beoordeling_3.setImageResource(R.drawable.sein_groen_s);
                        }
                        if(extractUsersT3.equals("2")) {
                            beoordeling_3.setImageResource(R.drawable.sein_geel_s);
                        }
                        if(extractUsersT3.equals("1")) {
                            beoordeling_3.setImageResource(R.drawable.sein_rood_s);
                        }
                        if(extractUsersT3.equals("0")) {
                            beoordeling_3.setImageResource(R.drawable.sein_geel_s);
                        }

                        if(extractUsersT4.equals("3")) {
                            beoordeling_4.setImageResource(R.drawable.sein_groen_s);
                        }
                        if(extractUsersT4.equals("2")) {
                            beoordeling_4.setImageResource(R.drawable.sein_geel_s);
                        }
                        if(extractUsersT4.equals("1")) {
                            beoordeling_4.setImageResource(R.drawable.sein_rood_s);
                        }
                        if(extractUsersT4.equals("0")) {
                            beoordeling_4.setImageResource(R.drawable.sein_geel_s);
                        }

                        if(extractUsersT5.equals("3")) {
                            beoordeling_5.setImageResource(R.drawable.sein_groen_s);
                        }
                        if(extractUsersT5.equals("2")) {
                            beoordeling_5.setImageResource(R.drawable.sein_geel_s);
                        }
                        if(extractUsersT5.equals("1")) {
                            beoordeling_5.setImageResource(R.drawable.sein_rood_s);
                        }
                        if(extractUsersT5.equals("0")) {
                            beoordeling_5.setImageResource(R.drawable.sein_geel_s);
                        }

                        if(extractUsersT6.equals("3")) {
                            beoordeling_6.setImageResource(R.drawable.sein_groen_s);
                        }
                        if(extractUsersT6.equals("2")) {
                            beoordeling_6.setImageResource(R.drawable.sein_geel_s);
                        }
                        if(extractUsersT6.equals("1")) {
                            beoordeling_6.setImageResource(R.drawable.sein_rood_s);
                        }
                        if(extractUsersT6.equals("0")) {
                            beoordeling_6.setImageResource(R.drawable.sein_geel_s);
                        }

                        if(extractUsersT7.equals("3")) {
                            beoordeling_7.setImageResource(R.drawable.sein_groen_s);
                        }
                        if(extractUsersT7.equals("2")) {
                            beoordeling_7.setImageResource(R.drawable.sein_geel_s);
                        }
                        if(extractUsersT7.equals("1")) {
                            beoordeling_7.setImageResource(R.drawable.sein_rood_s);
                        }
                        if(extractUsersT7.equals("0")) {
                            beoordeling_7.setImageResource(R.drawable.sein_geel_s);
                        }

                        if(extractUsersT8.equals("3")) {
                            beoordeling_8.setImageResource(R.drawable.sein_groen_s);
                        }
                        if(extractUsersT8.equals("2")) {
                            beoordeling_8.setImageResource(R.drawable.sein_geel_s);
                        }
                        if(extractUsersT8.equals("1")) {
                            beoordeling_8.setImageResource(R.drawable.sein_rood_s);
                        }
                        if(extractUsersT8.equals("0")) {
                            beoordeling_8.setImageResource(R.drawable.sein_geel_s);
                        }


                        your_array_list.add(extractUsers);

                    }

                    //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UsersActivity.this, android.R.layout.simple_list_item_1, your_array_list);
                    //listUsers.setAdapter(arrayAdapter);
                    //GetArrayValue();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters = new HashMap<String, String>();

                parameters.put("usertype", (pNummer));
                return parameters;
            }
        };

        requestQueue.add(request);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){

        MenuItem groep = menu.findItem(R.id.action_groepen_wijzigen);

        final String aantalGroepen = mSharedPreferences.getString(PREF_AANTALGROEPEN, "1");

        int aantal_groepen = Integer.parseInt(aantalGroepen);
        if(aantal_groepen > 1){
            groep.setVisible(true);
        }else {
            groep.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        final String functie = mSharedPreferences.getString(PREF_FUNCTIE, "");

        if(functie.equals("Team Manager")) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }else{
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            final String versionName = BuildConfig.VERSION_NAME;
            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
            SharedPreferences.Editor e = mSharedPreferences.edit().clear();
            e.putString(PREF_NEWINSTALL, versionName);
            e.commit();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.action_exit) {

            this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        if (id == R.id.action_groepen_wijzigen) {

            System.out.println("mentorengroep: "+ "input");

            Intent intent = new Intent(getApplicationContext(), MentorenGroepActivity.class);
            intent.putExtra("UserType", "leerling");
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }

}

