package org.bwis.nsmentorenapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mediacenter on 26-5-2016.
 */
public class BeoordelingMainActivity extends AppCompatActivity {
    private static final String PREFS = "prefs";
    private static final String PREF_NAME = "name";
    private static final String PREF_PNR = "id";
    private static final String PREF_ID = "id";
    private static final String PREF_FUNCTIE = "functie";
    private static final String PREF_VOORNAAM = "voornaam";
    private static final String PREF_TUSSENVOEGSEL = "tussenvoegsel";
    private static final String PREF_ACHTERNAAM = "achternaam";
    private static final String PREF_NEWINSTALL = "newInstall";
    private static final String PREF_AANTALGROEPEN = "aantalGroepen";
    private static final String PREF_MENTORENGROEP = "mentorenGroep";

    SharedPreferences mSharedPreferences;
    TextView tViewPnummer;
    TextView tViewNaam;
    TextView tViewFunctie;
    TextView tViewStandplaats;
    TextView tViewTelefoon;
    TextView tViewHeadTB;
    TextView tViewTips;
    TextView tViewTipMentor;

    ImageView iBeoordelingGroen, iBeoordelingGeel, iBeoordelingRood;
    Button beoordelingOpslaan;
    LinearLayout layoutWB, layoutGeschiedenis;
    final String tips = "0";
    private String pNummer;
    private String beoordeling_name;
    private String id;
    private String volNaam;
    String user_name = "";
    String TB, beoordelingType, beoordeling;

    TextView displayID;
    RequestQueue requestQueue;
    ArrayList<String> your_array_list = new ArrayList<String>();
    ImageView beoordeling_1;

    methods method = new methods();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beoordeling_activity_main);
        displayID = (TextView) findViewById(R.id.displayID);
        beoordeling_1 = (ImageView) findViewById(R.id.imageMat);
        tViewHeadTB = (TextView) findViewById(R.id.textViewHeadTB);
        layoutWB = (LinearLayout) findViewById(R.id.layoutWB);
        layoutGeschiedenis = (LinearLayout)findViewById(R.id.layoutGeschiedenis);

        Intent intent = getIntent();

        pNummer = intent.getStringExtra("personeelnummer");
        beoordeling_name = intent.getStringExtra("beoordeling");

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        id = mSharedPreferences.getString(PREF_ID, "");
        String functie = mSharedPreferences.getString(PREF_FUNCTIE, "");
        String voornaam = mSharedPreferences.getString(PREF_VOORNAAM, "");
        String tussenvoegsel = mSharedPreferences.getString(PREF_TUSSENVOEGSEL, "");
        String achternaam = mSharedPreferences.getString(PREF_ACHTERNAAM, "");
        final String mentorenGroep = mSharedPreferences.getString(PREF_MENTORENGROEP, "?");

        if(functie.equals("Mentor")){
            layoutWB.setVisibility(layoutWB.VISIBLE);
        }
        if(functie.equals("Team Manager")){
            layoutWB.setVisibility(layoutWB.VISIBLE);
        }

        String voledigeNaam = method.volnaam_method(voornaam, achternaam, tussenvoegsel);
        String functieNaam = method.functienaam_method(functie);

        displayID.setText(functieNaam + ": " + voledigeNaam + " - " + mentorenGroep);

        String GetUserDataUrl = "https://nspersoneel.nl/mentorenApp/GetUserData.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        if(beoordeling_name.equals("1")){
            tViewHeadTB.setText("Kennis van materieel");
            beoordelingType = "Kennis van materieel";
        }
        if(beoordeling_name.equals("2")){
            tViewHeadTB.setText("Kennis van seingeving");
            beoordelingType = "Kennis van seingeving";
        }
        if(beoordeling_name.equals("3")){
            tViewHeadTB.setText("Opvolging seinen");
            beoordelingType = "Opvolging seinen";
        }
        if(beoordeling_name.equals("4")){
            tViewHeadTB.setText("Rangeerproces");
            beoordelingType = "Rangeerproces";
        }
        if(beoordeling_name.equals("5")){
            tViewHeadTB.setText("Concentratie");
            beoordelingType = "Concentratie";
        }
        if(beoordeling_name.equals("6")){
            tViewHeadTB.setText("In/uit dienstmelden");
            beoordelingType = "In/uit dienstmelden";
        }
        if(beoordeling_name.equals("7")){
            tViewHeadTB.setText("Kleding en schoeisel");
            beoordelingType = "Kleding en schoeisel";
        }
        if(beoordeling_name.equals("8")){
            tViewHeadTB.setText("Houding en gedrag");
            beoordelingType = "Houding en gedrag";
        }

        //getBeoordelingsInfo();








        final Intent intent2 = new Intent(this, BeoordelingcodeActivity.class);

        layoutWB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                //startActivityForResult(intent2, 1);



                AlertDialog.Builder builder = new AlertDialog.Builder(BeoordelingMainActivity.this);

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.beoordeling_popup,null);

                builder.setView(dialogView);

                final ImageButton buttonGreen = (ImageButton)dialogView.findViewById(R.id.buttonGreen);
                final ImageButton buttonYellow = (ImageButton)dialogView.findViewById(R.id.buttonYellow);
                final ImageButton buttonRed = (ImageButton)dialogView.findViewById(R.id.buttonRed);
                final Button buttonClose = (Button)dialogView.findViewById(R.id.button_popup_close);


                final AlertDialog dialog = builder.create();

                buttonGreen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        onActivityResult4();
                    }
                });

                buttonYellow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        onActivityResult3();

                    }
                });

                buttonRed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        onActivityResult2();
                    }
                });

                buttonClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }

        });


        layoutGeschiedenis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), BeoordelingHistoryActivity.class);
                intent.putExtra("UserType", "leerling");
                intent.putExtra("type", beoordeling_name);
                intent.putExtra("lid", pNummer);
                intent.putExtra("userName", user_name);
                intent.putExtra("beoordelingType", beoordelingType);
                intent.putExtra("beoordeling", beoordeling);
                startActivity(intent);
            }
        });

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            String beoordelingCode = data.getStringExtra(BeoordelingcodeActivity.RESULT_BEOORDELING);
            //;
            if(beoordelingCode.equals("1")) {
                onActivityResult2();
            }
            if(beoordelingCode.equals("2")){
                //beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                //setBeoordelingToDB("2", "");
                onActivityResult3();
            }
            if(beoordelingCode.equals("3")){
                onActivityResult4();
            }
        }
    }

    public void onActivityResult2() {

        AlertDialog.Builder builder = new AlertDialog.Builder(BeoordelingMainActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.enter_beoordeling_popup,null);

        builder.setView(dialogView);

        final Button tipOpslaan = (Button)dialogView.findViewById(R.id.tipOpslaan);
        final Button buttonClose = (Button)dialogView.findViewById(R.id.button_popup_close);
        final EditText tipText = (EditText)dialogView.findViewById(R.id.tipText);
        //final ImageButton buttonRed = (ImageButton)dialogView.findViewById(R.id.buttonRed);

        buttonClose.setAlpha(.4f);
        buttonClose.setEnabled(false);

        final AlertDialog dialog = builder.create();

        tipOpslaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tips_text = tipText.getText().toString();

                if (tips_text.equals("")) {
                    onActivityResult2();
                    Toast.makeText(BeoordelingMainActivity.this, "Als U een beoordeling 'nog niet voldoende' geeft moet u ook onderbouwen waarom dit zo is.",
                            Toast.LENGTH_LONG).show();

                } else {
                    setBeoordelingToDB("1", tips_text);

                    dialog.dismiss();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }
        });

        dialog.show();

    }

    public void onActivityResult3() {

        AlertDialog.Builder builder = new AlertDialog.Builder(BeoordelingMainActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.enter_beoordeling_popup,null);

        builder.setView(dialogView);

        final Button tipOpslaan = (Button)dialogView.findViewById(R.id.tipOpslaan);
        final Button buttonClose = (Button)dialogView.findViewById(R.id.button_popup_close);
        final EditText tipText = (EditText)dialogView.findViewById(R.id.tipText);
        //final ImageButton buttonRed = (ImageButton)dialogView.findViewById(R.id.buttonRed);



        final AlertDialog dialog = builder.create();

        tipOpslaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tips_text = tipText.getText().toString();
                setBeoordelingToDB("2", tips_text);

                dialog.dismiss();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setBeoordelingToDB("2", "");

                dialog.dismiss();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
        dialog.show();

    }

    public void onActivityResult4() {

        AlertDialog.Builder builder = new AlertDialog.Builder(BeoordelingMainActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.enter_beoordeling_popup,null);

        builder.setView(dialogView);

        final Button tipOpslaan = (Button)dialogView.findViewById(R.id.tipOpslaan);
        final Button buttonClose = (Button)dialogView.findViewById(R.id.button_popup_close);
        final EditText tipText = (EditText)dialogView.findViewById(R.id.tipText);
        //final ImageButton buttonRed = (ImageButton)dialogView.findViewById(R.id.buttonRed);



        final AlertDialog dialog = builder.create();

        tipOpslaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tips_text = tipText.getText().toString();
                setBeoordelingToDB("3", tips_text);

                dialog.dismiss();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setBeoordelingToDB("3", "");

                dialog.dismiss();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
        dialog.show();

    }

    public void setBeoordelingToDB(final String value, final String tipsText) {

        String setBeoordelingTODBUrl = "https://nspersoneel.nl/mentorenApp/addBeoordelingToDB.php";
        HttpsTrustManager.allowAllSSL();
        StringRequest request = new StringRequest(Request.Method.POST,setBeoordelingTODBUrl, new Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                try {
                    JSONObject jsnobject = new JSONObject(response);

                    JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");



                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject statusObject = jsonArray.getJSONObject(i);

                        String extractUsersAchternaam = statusObject.getString("achternaam");


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters = new HashMap<String, String>();

                parameters.put("aspirant_pn", (pNummer));
                parameters.put("mentor_pn", (id));
                parameters.put("btype", (beoordeling_name));
                parameters.put("bvalue", (value));
                parameters.put("tips", (tipsText));
                return parameters;
            }
        };

        requestQueue.add(request);
    }
    public void getBeoordelingsInfo(){
        String GetUserDataUrl = "https://nspersoneel.nl/mentorenApp/GetUserData.php";
        HttpsTrustManager.allowAllSSL();
        StringRequest request = new StringRequest(Request.Method.POST,GetUserDataUrl, new Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                try {
                    JSONObject jsnobject = new JSONObject(response);

                    JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");



                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject statusObject = jsonArray.getJSONObject(i);

                        String extractUsersAchternaam = statusObject.getString("achternaam");
                        String extractUsersVoornaam = statusObject.getString("voornaam");
                        String extractUsersTussenvoegsel = statusObject.getString("tussenvoegsel");
                        String extractUsersTelefoon = statusObject.getString("telefoon");
                        String extractUsersPersoneelsnummer = statusObject.getString("persooneelsnummer");
                        String extractUsersFunctie = statusObject.getString("functie");
                        String extractUsersStandplaats = statusObject.getString("standplaats");
                        String extractUsersT1 = statusObject.getString("T1");
                        String extractUsersT2 = statusObject.getString("T2");
                        String extractUsersT3 = statusObject.getString("T3");
                        String extractUsersT4 = statusObject.getString("T4");
                        String extractUsersT5 = statusObject.getString("T5");
                        String extractUsersT6 = statusObject.getString("T6");
                        String extractUsersT7 = statusObject.getString("T7");
                        String extractUsersT8 = statusObject.getString("T8");
                        String extractUsersT9 = statusObject.getString("T9");
                        String extractUsersT10 = statusObject.getString("T10");
                        String extractUsersT11 = statusObject.getString("T11");
                        String extractUsersT12 = statusObject.getString("T12");
                        String extractUsersT13 = statusObject.getString("T13");
                        String extractUsersT14 = statusObject.getString("T14");
                        String extractUsersT15 = statusObject.getString("T15");
                        String extractTips = statusObject.getString("tips");
                        String extractMentorNaam = statusObject.getString("mentor_naam");
                        String extractDatum = statusObject.getString("tip_datum");

                        String extractUsers;

                        if(extractUsersTussenvoegsel.equals("NULL")){
                            extractUsers = extractUsersVoornaam + " " + extractUsersAchternaam;
                        }else{
                            extractUsers = extractUsersVoornaam + " " + extractUsersTussenvoegsel + " " + extractUsersAchternaam;
                        }

                        tViewNaam = (TextView) findViewById(R.id.textViewNaam);
                        tViewNaam.setText(extractUsers);
                        user_name = extractUsers;

                        if(extractTips.equals("0")){

                        }else{
                            tViewTips = (TextView) findViewById(R.id.textViewTips);
                            tViewTips.setText(extractTips + "\n");

                            tViewTipMentor = (TextView) findViewById(R.id.textViewTipMentor);
                            tViewTipMentor.setText("Mentor: " + extractMentorNaam + " op " + extractDatum);
                        }

                        if(beoordeling_name.equals("1")){

                            beoordeling = extractUsersT1;

                            if(extractUsersT1.equals("3")) {
                                beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                            }
                            if(extractUsersT1.equals("2")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                            if(extractUsersT1.equals("1")) {
                                beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                            }
                            if(extractUsersT1.equals("0")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                        }
                        if(beoordeling_name.equals("2")){

                            beoordeling = extractUsersT2;

                            if(extractUsersT2.equals("3")) {
                                beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                            }
                            if(extractUsersT2.equals("2")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                            if(extractUsersT2.equals("1")) {
                                beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                            }
                            if(extractUsersT2.equals("0")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                        }
                        if(beoordeling_name.equals("3")){

                            beoordeling = extractUsersT3;

                            if(extractUsersT3.equals("3")) {
                                beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                            }
                            if(extractUsersT3.equals("2")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                            if(extractUsersT3.equals("1")) {
                                beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                            }
                            if(extractUsersT3.equals("0")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                        }
                        if(beoordeling_name.equals("4")){

                            beoordeling = extractUsersT4;

                            if(extractUsersT4.equals("3")) {
                                beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                            }
                            if(extractUsersT4.equals("2")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                            if(extractUsersT4.equals("1")) {
                                beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                            }
                            if(extractUsersT4.equals("0")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                        }
                        if(beoordeling_name.equals("5")){

                            beoordeling = extractUsersT5;

                            if(extractUsersT5.equals("3")) {
                                beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                            }
                            if(extractUsersT5.equals("2")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                            if(extractUsersT5.equals("1")) {
                                beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                            }
                            if(extractUsersT5.equals("0")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                        }
                        if(beoordeling_name.equals("6")){

                            beoordeling = extractUsersT6;

                            if(extractUsersT6.equals("3")) {
                                beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                            }
                            if(extractUsersT6.equals("2")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                            if(extractUsersT6.equals("1")) {
                                beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                            }
                            if(extractUsersT6.equals("0")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                        }
                        if(beoordeling_name.equals("7")){

                            beoordeling = extractUsersT7;

                            if(extractUsersT7.equals("3")) {
                                beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                            }
                            if(extractUsersT7.equals("2")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                            if(extractUsersT7.equals("1")) {
                                beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                            }
                            if(extractUsersT7.equals("0")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                        }
                        if(beoordeling_name.equals("8")){

                            beoordeling = extractUsersT8;

                            if(extractUsersT8.equals("3")) {
                                beoordeling_1.setImageResource(R.drawable.sein_groen_s);
                            }
                            if(extractUsersT8.equals("2")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                            if(extractUsersT8.equals("1")) {
                                beoordeling_1.setImageResource(R.drawable.sein_rood_s);
                            }
                            if(extractUsersT8.equals("0")) {
                                beoordeling_1.setImageResource(R.drawable.sein_geel_s);
                            }
                        }

                        your_array_list.add(extractUsers);

                    }

                    //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UsersActivity.this, android.R.layout.simple_list_item_1, your_array_list);
                    //listUsers.setAdapter(arrayAdapter);
                    //GetArrayValue();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters = new HashMap<String, String>();

                parameters.put("usertype", (pNummer));
                parameters.put("btype", (beoordeling_name));
                return parameters;
            }
        };

        requestQueue.add(request);

    }
    @Override
    public void onResume()
    {
        super.onResume();
        getBeoordelingsInfo();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){

        MenuItem groep = menu.findItem(R.id.action_groepen_wijzigen);

        final String aantalGroepen = mSharedPreferences.getString(PREF_AANTALGROEPEN, "1");

        int aantal_groepen = Integer.parseInt(aantalGroepen);
        if(aantal_groepen > 1){
            groep.setVisible(true);
        }else {
            groep.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            final String versionName = BuildConfig.VERSION_NAME;
            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
            SharedPreferences.Editor e = mSharedPreferences.edit().clear();
            e.putString(PREF_NEWINSTALL, versionName);
            e.commit();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.action_exit) {

            this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        if (id == R.id.action_groepen_wijzigen) {

            System.out.println("mentorengroep: "+ "input");

            Intent intent = new Intent(getApplicationContext(), MentorenGroepActivity.class);
            intent.putExtra("UserType", "leerling");
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onRestart() {
        super.onRestart();
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}
