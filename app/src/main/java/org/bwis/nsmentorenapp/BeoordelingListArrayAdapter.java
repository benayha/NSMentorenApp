package org.bwis.nsmentorenapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mediacenter on 1-6-2016.
 */
public class BeoordelingListArrayAdapter extends ArrayAdapter<BeoordelingcodeActivity.Beoordeling> {

    private final List<BeoordelingcodeActivity.Beoordeling> list;
    private final Activity context;

    static class ViewHolder {
        protected TextView name;
        protected ImageView flag;
    }

    public BeoordelingListArrayAdapter(Activity context, List<BeoordelingcodeActivity.Beoordeling> list) {
        super(context, R.layout.list_row_beoordeling, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.list_row_beoordeling, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.name2);
            viewHolder.flag = (ImageView) view.findViewById(R.id.flag);
              view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText(list.get(position).getName());
        holder.flag.setImageDrawable(list.get(position).getFlag());
        return view;
    }
}
