package org.bwis.nsmentorenapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mediacenter on 26-5-2016.
 */
public class BeoordelingHistoryActivity extends AppCompatActivity {

    TextView displayID, textViewNaam, heading;
    ImageView imageBeoordeling;
    ListView listUsers;
    ArrayList<String> UsersList;
    RequestQueue requestQueue;
    ArrayList<String> your_array_list = new ArrayList<String>();

    private static final String PREFS = "prefs";
    private static final String PREF_NAME = "name";
    private static final String PREF_PNR = "id";
    private static final String PREF_ID = "id";
    private static final String PREF_FUNCTIE = "functie";
    private static final String PREF_VOORNAAM = "voornaam";
    private static final String PREF_TUSSENVOEGSEL = "tussenvoegsel";
    private static final String PREF_ACHTERNAAM = "achternaam";
    private static final String PREF_NEWINSTALL = "newInstall";
    private static final String PREF_AANTALGROEPEN = "aantalGroepen";
    private static final String PREF_MENTORENGROEP = "mentorenGroep";


    SharedPreferences mSharedPreferences;

    methods method = new methods();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_history_main);

        Intent intent = getIntent();
        final String type = intent.getStringExtra("type");
        final String lid = intent.getStringExtra("lid");
        final String userType = intent.getStringExtra("userType");
        final String userName = intent.getStringExtra("userName");
        final String beoordelingType = intent.getStringExtra("beoordelingType");
        final String beoordeling = intent.getStringExtra("beoordeling");
        String headerText= "";

        //Toast.makeText(BeoordelingHistoryActivity.this, "dit is een test" + lid + type, Toast.LENGTH_SHORT).show();

        final ListView lv1 = (ListView) findViewById(R.id.listTips);

        displayID = (TextView) findViewById(R.id.displayID);
        textViewNaam = (TextView)findViewById(R.id.textViewNaam);
        heading = (TextView)findViewById(R.id.heading);
        imageBeoordeling = (ImageView)findViewById(R.id.imageBeoordeling);

        //listUsers = (ListView) findViewById(R.id.listUsers);
        String GetUsersUrl = "https://nspersoneel.nl/mentorenApp/GetUserHistory.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        heading.setText(beoordelingType);

        if(beoordeling.equals("3")) {
            imageBeoordeling.setImageResource(R.drawable.sein_groen_s);
        }
        if(beoordeling.equals("2")) {
            imageBeoordeling.setImageResource(R.drawable.sein_geel_s);
        }
        if(beoordeling.equals("1")) {
            imageBeoordeling.setImageResource(R.drawable.sein_rood_s);
        }
        if(beoordeling.equals("0")) {
            imageBeoordeling.setImageResource(R.drawable.sein_geel_s);
        }

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        String id = mSharedPreferences.getString(PREF_ID, "");
        final String functie = mSharedPreferences.getString(PREF_FUNCTIE, "");
        String voornaam = mSharedPreferences.getString(PREF_VOORNAAM, "");
        String tussenvoegsel = mSharedPreferences.getString(PREF_TUSSENVOEGSEL, "");
        String achternaam = mSharedPreferences.getString(PREF_ACHTERNAAM, "");
        final String mentorenGroep = mSharedPreferences.getString(PREF_MENTORENGROEP, "?");
        final String volNaam;

        final String voledigeNaam = method.volnaam_method(voornaam, achternaam, tussenvoegsel);
        String functieNaam = method.functienaam_method(functie);

        displayID.setText(functieNaam + ": " + voledigeNaam + " - " + mentorenGroep);
        textViewNaam.setText(userName);


        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String out = your_array_list.get(position);
                String [] tid_split = out.split(":");

                final String tid = tid_split[2];
                String edittext = tid_split[0];

                final String editor = tid_split[1];

                String [] edittext_split = edittext.split("!!");
                final String editText = edittext_split[1];
                final String beoordelingCode = edittext_split[2];

                AlertDialog.Builder builder = new AlertDialog.Builder(BeoordelingHistoryActivity.this);

                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.history_popup,null);

                builder.setView(dialogView);

                final TextView popup_text = (TextView) dialogView.findViewById(R.id.text_pop);
                final Button closeBtn = (Button)dialogView.findViewById(R.id.button_popup_close);
                final Button editBtn = (Button)dialogView.findViewById(R.id.button_popup_edit);
                final Button deleteBtn = (Button)dialogView.findViewById(R.id.button_popup_delete);
                final ImageView historyPopupImage = (ImageView)dialogView.findViewById(R.id.history_popup_image);

                popup_text.setText(editText);

                if(beoordelingCode.equals("3")) {
                    historyPopupImage.setImageResource(R.drawable.sein_groen_s);
                }
                if(beoordelingCode.equals("2")) {
                    historyPopupImage.setImageResource(R.drawable.sein_geel_s);
                }
                if(beoordelingCode.equals("1")) {
                    historyPopupImage.setImageResource(R.drawable.sein_rood_s);
                }

                popup_text.setMovementMethod(new ScrollingMovementMethod());

                final AlertDialog dialog = builder.create();

                closeBtn .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                editBtn .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        AlertDialog.Builder builder = new AlertDialog.Builder(BeoordelingHistoryActivity.this);

                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.beoordeling_history_menu,null);

                        builder.setView(dialogView);

                        final Button cancelBtn = (Button)dialogView.findViewById(R.id.button_editHistory_cancel);
                        final Button saveBtn = (Button)dialogView.findViewById(R.id.button_editHistory_save);
                        final TextView textViewHistoryEdit = (TextView)dialogView.findViewById(R.id.textViewEditHistory);

                        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
                        edt.setText(editText);
                        textViewHistoryEdit.setText("U kunt hier uw beoordeling verbeteren!");

                        final AlertDialog dialog = builder.create();

                        cancelBtn .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        saveBtn .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String tips = edt.getText().toString();

                                if (tips.equals("")) {

                                    Toast.makeText(BeoordelingHistoryActivity.this, "U mag geen lege beoordeling geven.",
                                            Toast.LENGTH_LONG).show();

                                } else {

                                    editTipinDB(tid, tips);
                                }
                            }
                        });
                        dialog.show();
                    }
                });

                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteTip(tid, type);
                    }
                });

                if(functie.equals("Team Manager")){

                }

                if(functie.equals("Mentor")){
                    String editorC = editor.replace(" ", "");
                    String volNaamC = voledigeNaam.replace(" ", "");

                    if(!editorC.equals(volNaamC)){

                        editBtn.setAlpha(.4f);
                        editBtn.setEnabled(false);
                        deleteBtn.setAlpha(.4f);
                        deleteBtn.setEnabled(false);

                    }

                }

                if(functie.equals("Leerling")){

                    editBtn.setAlpha(.4f);
                    editBtn.setEnabled(false);
                    deleteBtn.setAlpha(.4f);
                    deleteBtn.setEnabled(false);

                }

                dialog.show();

            }

        });

        HttpsTrustManager.allowAllSSL();
        StringRequest request = new StringRequest(Request.Method.POST,GetUsersUrl, new Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                try {
                    JSONObject jsnobject = new JSONObject(response);

                    JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");



                    for (int i = 0; i < jsonArray.length(); i++) {


                        JSONObject statusObject = jsonArray.getJSONObject(i);

                        String extractUsersAchternaam = statusObject.getString("datum");
                        String extractUsersVoornaam = statusObject.getString("tips");
                        String extractUsersTussenvoegsel = statusObject.getString("type_value");
                        String extractUsersTelefoon = statusObject.getString("mid");
                        String extractTid = statusObject.getString("tid");

                        String extractUsers;
                        if(extractUsersVoornaam.equals("NULL")){
                            extractUsersVoornaam = " ";
                        }
/*
                        if(extractUsersTussenvoegsel.equals("NULL")){
                            extractUsers = extractUsersVoornaam + " " + extractUsersAchternaam + ":t;" + extractUsersTelefoon + ":p;" + extractUsersPersoneelsnummer;
*/
                            extractUsers = extractUsersAchternaam + "!!" + extractUsersVoornaam + "!!" + extractUsersTussenvoegsel + ":" + extractUsersTelefoon + ":" + extractTid;


                        //extractUsers = extractUsersVoornaam + extractUsersTussenvoegsel + extractUsersTelefoon;
                         your_array_list.add(extractUsers);

                    }

                    //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UsersActivity.this, android.R.layout.simple_list_item_1, your_array_list);
                    //listUsers.setAdapter(arrayAdapter);
                    GetArrayValue();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters = new HashMap<String, String>();
                parameters.put("type", (type));
                parameters.put("lid", (lid));
                return parameters;
            }
        };
        requestQueue.add(request);






    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){

        MenuItem groep = menu.findItem(R.id.action_groepen_wijzigen);

        final String aantalGroepen = mSharedPreferences.getString(PREF_AANTALGROEPEN, "1");

        int aantal_groepen = Integer.parseInt(aantalGroepen);
        if(aantal_groepen > 1){
            groep.setVisible(true);
        }else {
            groep.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            final String versionName = BuildConfig.VERSION_NAME;
            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
            SharedPreferences.Editor e = mSharedPreferences.edit().clear();
            e.putString(PREF_NEWINSTALL, versionName);
            e.commit();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.action_exit) {

            this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        if (id == R.id.action_groepen_wijzigen) {

            System.out.println("mentorengroep: "+ "input");

            Intent intent = new Intent(getApplicationContext(), MentorenGroepActivity.class);
            intent.putExtra("UserType", "leerling");
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }

    private void GetArrayValue(){

        ArrayList image_details = getListData();
        final ListView lv1 = (ListView) findViewById(R.id.listTips);
        lv1.setAdapter(new CustomListHistoryAdapter(this, image_details));
    }

    private ArrayList getListData() {


        ArrayList<NewsItem> results = new ArrayList<NewsItem>();

        for (int i = 0; i <your_array_list.size(); i++) {


            NewsItem newsData = new NewsItem();
            String[] separated = your_array_list.get(i).split(":");

            String[] firstpart = separated[0].split("!!");
            String datum = firstpart[0];
            String tip = firstpart[1];
            String beoordeling = firstpart[2];
            String mentor = separated[1];

            if(beoordeling.equals("3")){
                newsData.setPic("3");
            }
            if(beoordeling.equals("2")){
                newsData.setPic("2");
            }
            if(beoordeling.equals("1")){
                newsData.setPic("1");
            }
            if(tip.equals("NULL")){
                tip = "---";
            }

            newsData.setHeadline(tip);

            newsData.setReporterName("Van: " + mentor + "\nOp: " + datum);

            results.add(newsData);
        }

        // Add some more dummy data for testing
        return results;

    }

     private void deleteTip(String tipId, final String tipType){



         final String tipIdDB = tipId;
         String DeleteTipUrl = "https://nspersoneel.nl/mentorenApp/DeleteBeoordelingFromDB.php";
         HttpsTrustManager.allowAllSSL();
         StringRequest request = new StringRequest(Request.Method.POST,DeleteTipUrl, new Response.Listener<String>(){
             @Override
             public void onResponse(String response){
                 try {
                     JSONObject jsnobject = new JSONObject(response);

                     JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");



                     for (int i = 0; i < jsonArray.length(); i++) {


                         JSONObject statusObject = jsonArray.getJSONObject(i);

                         String extractUsersTelefoon = statusObject.getString("mid");

                         finish();
                         startActivity(getIntent());
                     }


                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
             }
         }, new Response.ErrorListener(){
             @Override
             public void onErrorResponse(VolleyError error){
             }
         }){
             @Override
             protected Map<String, String> getParams() throws AuthFailureError {
                 Map<String,String> parameters = new HashMap<String, String>();
                 parameters.put("type", ("Delete"));
                 parameters.put("tipIdDB", (tipIdDB));
                 parameters.put("tipType", (tipType));
                 return parameters;
             }
         };
         requestQueue.add(request);

     }

     public void editTipinDB(final String tipIdDBdel, final String tip){

         if (tip.equals("")) {

             Toast.makeText(BeoordelingHistoryActivity.this, "Als U een beoordeling 'nog niet voldoende' geeft moet u ook onderbouwen waarom dit zo is.",
                     Toast.LENGTH_LONG).show();

         } else {

             String DeleteTipUrl = "https://nspersoneel.nl/mentorenApp/UpdateBeoordelingInDB.php";
             HttpsTrustManager.allowAllSSL();
             StringRequest request = new StringRequest(Request.Method.POST,DeleteTipUrl, new Response.Listener<String>(){
                 @Override
                 public void onResponse(String response){
                     try {
                         JSONObject jsnobject = new JSONObject(response);

                         JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");



                         for (int i = 0; i < jsonArray.length(); i++) {


                             JSONObject statusObject = jsonArray.getJSONObject(i);

                         }



                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                     finish();
                     startActivity(getIntent());
                 }
             }, new Response.ErrorListener(){
                 @Override
                 public void onErrorResponse(VolleyError error){
                 }
             }){
                 @Override
                 protected Map<String, String> getParams() throws AuthFailureError {
                     Map<String,String> parameters = new HashMap<String, String>();
                     parameters.put("tip", (tip));
                     parameters.put("tipIdDBdel", (tipIdDBdel));
                     //parameters.put("tipType", (tipType));
                     return parameters;
                 }
             };
             requestQueue.add(request);

         }

     }


}

