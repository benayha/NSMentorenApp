package org.bwis.nsmentorenapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.bwis.nsmentorenapp.LoginActivity;
import org.bwis.nsmentorenapp.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    private static final String PREFS = "prefs";
    private static final String PREF_NAME = "name";
    private static final String PREF_PNR = "id";
    private static final String PREF_ID = "id";
    private static final String PREF_FUNCTIE = "functie";
    private static final String PREF_VOORNAAM = "voornaam";
    private static final String PREF_TUSSENVOEGSEL = "tussenvoegsel";
    private static final String PREF_ACHTERNAAM = "achternaam";
    private static final String PREF_NEWINSTALL = "newInstall";
    private static final String PREF_MENTORENGROEP = "mentorenGroep";

    SharedPreferences mSharedPreferences;

    TextView displayID;

    RequestQueue requestQueue;

    final String vName = BuildConfig.VERSION_NAME;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appVInDB();
        checkForUpdates();

        displayID = (TextView) findViewById(R.id.displayID);


    }

    @Override
    public void onResume(){
        super.onResume();

    }

    public void checkForUpdates(){

        String loginUrl = "https://nspersoneel.nl/mentorenApp/checkForUpdates.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        final String id = mSharedPreferences.getString(PREF_ID, "null");

        HttpsTrustManager.allowAllSSL();
        System.out.println("add appVersie in DB " + vName + " " + id);

        StringRequest request = new StringRequest(Request.Method.POST, loginUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



                System.out.println("Update Message: " + response + " - " + vName);
                if(response.equals("\"" + vName + "\"")){
                    System.out.println("App don't need to update");
                    userKeuze();
                }else{
                    System.out.println("App needs to update");
                    updatePopUp();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No Connection");
                userKeuze();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("appVersie", (vName));
                parameters.put("pnr", (id));
                return parameters;
            }
        };
        requestQueue.add(request);
    }

    public void updatePopUp(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.update_popup,null);

        builder.setView(dialogView);

        final TextView popup_text = (TextView) dialogView.findViewById(R.id.text_pop);
        final Button closeBtn = (Button)dialogView.findViewById(R.id.button_popup_close);
        final Button toGooglePlay = (Button)dialogView.findViewById(R.id.button_naar_playstore);

        final AlertDialog dialog = builder.create();

        closeBtn .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                userKeuze();
            }
        });
        toGooglePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                userKeuze();
                Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                i.setData(Uri.parse("market://details?id=org.bwis.nsmentorenapp"));
                startActivity(i);
            }
        });
        dialog.show();

    }

    public void userKeuze(){

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        String id = mSharedPreferences.getString(PREF_ID, "");
        String functie = mSharedPreferences.getString(PREF_FUNCTIE, "");
        String voornaam = mSharedPreferences.getString(PREF_VOORNAAM, "");
        String tussenvoegsel = mSharedPreferences.getString(PREF_TUSSENVOEGSEL, "");
        String achternaam = mSharedPreferences.getString(PREF_ACHTERNAAM, "");
        String newInstall = mSharedPreferences.getString(PREF_NEWINSTALL, "");
        String volNaam;

        if(tussenvoegsel == "" ){
            volNaam = voornaam + " " + achternaam;
        }else{
            volNaam = voornaam + " " + tussenvoegsel + " " + achternaam;
        }
        displayID.setText(functie + ": " + volNaam);

        if(id == ""){

            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(intent);
            finish();
        }
        if(functie.equals("Team Manager")){
            Intent intent = new Intent(getApplicationContext(),tmMainActivity.class);
            startActivity(intent);
            finish();
        }
        if(functie.equals("Mentor")){
            Intent intent = new Intent(getApplicationContext(),mentorenMainActivity.class);
            startActivity(intent);
            finish();
        }
        if(functie.equals("Leerling")){
            Intent intent = new Intent(getApplicationContext(),leerlingenMainActivity.class);
            startActivity(intent);
            finish();
        }
        if(functie.equals("Stagair")){
            Intent intent = new Intent(getApplicationContext(),StagairMainActivity.class);
            startActivity(intent);
            finish();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(PREF_ID, "");
            e.commit();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.action_exit) {

            this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        if (id == R.id.action_groepen_wijzigen) {

            System.out.println("mentorengroep: "+ "input");

            mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(PREF_MENTORENGROEP, "Ddr1");
            e.commit();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            finish();

        }
        return super.onOptionsItemSelected(item);
    }

    public void appVInDB(){

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        String id = mSharedPreferences.getString(PREF_ID, "");

        System.out.println("add appVersie in DB " + vName + " " + id);

        if(!id.equals("")){
            System.out.println("add appVersie in DB " + vName + " " + id);

        }

    }
}