package org.bwis.nsmentorenapp;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mediacenter on 1-6-2016.
 */
public class BeoordelingcodeActivity extends ListActivity {

    public static String RESULT_BEOORDELING = "beoordelingcode";
    public String[] beoordeling_namen, beoordeling_punten;
    private TypedArray imgs;
    private List<Beoordeling> beoordelingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        populateBeoordelingList();
        ArrayAdapter<BeoordelingcodeActivity.Beoordeling> adapter = new BeoordelingListArrayAdapter(this, beoordelingList);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Beoordeling c = beoordelingList.get(position);
                Intent returnIntent = new Intent();
                returnIntent.putExtra(RESULT_BEOORDELING, c.getCode());
                setResult(RESULT_OK, returnIntent);
                imgs.recycle(); //recycle images
                finish();
            }
        });
    }

    private void populateBeoordelingList() {
        beoordelingList = new ArrayList<Beoordeling>();
        beoordeling_namen = getResources().getStringArray(R.array.beoordeling_namen);
        beoordeling_punten = getResources().getStringArray(R.array.beoordeling_punten);
        imgs = getResources().obtainTypedArray(R.array.beoordeling_sein);
        for(int i = 0; i < beoordeling_punten.length; i++){
            beoordelingList.add(new Beoordeling(beoordeling_namen[i], beoordeling_punten[i], imgs.getDrawable(i)));
        }
    }

    public class Beoordeling {
        private String name;
        private String code;
        private Drawable flag;
        public Beoordeling(String name, String code, Drawable flag){
            this.name = name;
            this.code = code;
            this.flag = flag;
        }
        public String getName() {
            return name;
        }
        public Drawable getFlag() {
            return flag;
        }
        public String getCode() {
            return code;
        }
    }
}
