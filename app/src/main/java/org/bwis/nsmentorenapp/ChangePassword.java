package org.bwis.nsmentorenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by benay on 25-6-2016.
 */
public class ChangePassword extends AppCompatActivity{

    private static final String PREFS = "prefs";
    private static final String PREF_FUNCTIE = "functie";
    private static final String PREF_ID = "id";
    private static final String PREF_VOORNAAM = "voornaam";
    private static final String PREF_TUSSENVOEGSEL = "tussenvoegsel";
    private static final String PREF_ACHTERNAAM = "achternaam";
    SharedPreferences mSharedPreferences;
    EditText passText1, passText2;
    Button changePassword;
    TextView errorMessage;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);

        Intent intent = getIntent();
        final String pnr = intent.getStringExtra("pnr");
        final String functie = intent.getStringExtra("functie");
        final String voornaam = intent.getStringExtra("voornaam");
        final String tussenvoegsel = intent.getStringExtra("tussenvoegsel");
        final String achternaam = intent.getStringExtra("achternaam");

        passText1 = (EditText) findViewById(R.id.passText1);
        passText2 = (EditText) findViewById(R.id.passText2);
        changePassword = (Button) findViewById(R.id.changePassword);
        errorMessage = (TextView) findViewById(R.id.errorMessage);

        changePassword.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                if(!passText1.getText().toString().equals(passText2.getText().toString())){
                    errorMessage.setText("De wachtwoorden komen niet overeen.");
                }else{
                    if(!passText1.getText().toString().equals("")){
                        errorMessage.setText("");
                        changePasswordInDB(pnr);
                    }else{
                        errorMessage.setText("Dit wachtwoord is niet geldig");
                    }
                }

            }
        });
    }
    public void changePasswordInDB(final String pnr){

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        String changePasswordUrl = "https://nspersoneel.nl/mentorenApp/changepassword.php";
        HttpsTrustManager.allowAllSSL();
        StringRequest request = new StringRequest(Request.Method.POST,changePasswordUrl, new Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                try {
                    JSONObject jsnobject = new JSONObject(response);

                    JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject statusObject = jsonArray.getJSONObject(i);
                        String statusMessage = statusObject.getString("resultCode");


                        if (statusMessage.equals("1")) {

                            Toast.makeText(ChangePassword.this, "U wachtwoord is gewijzigd!!!",
                                    Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();

                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters = new HashMap<String, String>();
                parameters.put("passtext", passText1.getText().toString());
                parameters.put("pnr", pnr);
                return parameters;
            }
        };
        requestQueue.add(request);
    }

}
