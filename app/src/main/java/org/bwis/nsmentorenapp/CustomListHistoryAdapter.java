package org.bwis.nsmentorenapp;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mediacenter on 2-3-2016.
 */
public class CustomListHistoryAdapter extends BaseAdapter {
    private ArrayList<NewsItem> listData;
    private LayoutInflater layoutInflater;

    public CustomListHistoryAdapter(Context aContext, ArrayList<NewsItem> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_history_layout, null);
            holder = new ViewHolder();
            holder.headlineView = (TextView) convertView.findViewById(R.id.title);
            holder.reporterNameView = (TextView) convertView.findViewById(R.id.intro);
           // holder.beoordelingView = (TextView) convertView.findViewById(R.id.beoordeling);
            holder.imageView = (ImageView) convertView.findViewById(R.id.picture);

            //holder.reportedDateView = (TextView) convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.headlineView.setText(listData.get(position).getHeadline());
        holder.reporterNameView.setText(listData.get(position).getReporterName());
        //holder.beoordelingView.setText(listData.get(position).getBeoordeling());

        if(holder.pic.format(listData.get(position).getPic()).equals("3")) {
            holder.imageView.setImageResource(R.drawable.sein_groen_s);
        }
        if(holder.pic.format(listData.get(position).getPic()).equals("2")) {
            holder.imageView.setImageResource(R.drawable.sein_geel_s);
        }
        if(holder.pic.format(listData.get(position).getPic()).equals("1")) {
            holder.imageView.setImageResource(R.drawable.sein_rood_s);
        }
        //holder.reportedDateView.setText(listData.get(position).getDate());
        return convertView;
    }

    static class ViewHolder {
        TextView headlineView;
        ImageView imageView;
        TextView reporterNameView;
        //TextView beoordelingView;
        String pic;

        //TextView reportedDateView;
    }


}
