package org.bwis.nsmentorenapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by benay on 21-11-2016.
 */
public class methods extends AppCompatActivity {




    public String volnaam_method(String voornaam, String achternaam, String tussenvoegsel){

        String volNaam;

        if(tussenvoegsel == "" ){
            volNaam = voornaam + " " + achternaam;
        }else{
            volNaam = voornaam + " " + tussenvoegsel + " " + achternaam;
        }

        return(volNaam);

    }

    public String functienaam_method(String functie){
        if (functie.equals("Leerling")){
            return("Aspirant");
        }else{
            return(functie);
        }
    }

    public void updatePopUp(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.update_popup,null);

        builder.setView(dialogView);

        final TextView popup_text = (TextView) dialogView.findViewById(R.id.text_pop);
        final Button closeBtn = (Button)dialogView.findViewById(R.id.button_popup_close);

        final AlertDialog dialog = builder.create();

        closeBtn .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
