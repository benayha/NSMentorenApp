package org.bwis.nsmentorenapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class LoginActivity extends AppCompatActivity {

    private static final String PREFS = "prefs";
    private static final String PREF_FUNCTIE = "functie";
    private static final String PREF_ID = "id";
    private static final String PREF_VOORNAAM = "voornaam";
    private static final String PREF_TUSSENVOEGSEL = "tussenvoegsel";
    private static final String PREF_ACHTERNAAM = "achternaam";
    private static final String PREF_NEWINSTALL = "newInstall";
    private static final String PREF_MENTORENGROEP = "mentorenGroep";
    private static final String PREF_AANTALGROEPEN = "aantalGroepen";
    SharedPreferences mSharedPreferences;

    EditText loginText, passText;
    Button login;
    TextView errorMessage;
    RequestQueue requestQueue;

    String loginUrl = "https://nspersoneel.nl/mentorenApp/appLogin.php";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginText = (EditText) findViewById(R.id.loginText);
        passText = (EditText) findViewById(R.id.passText);
        login = (Button) findViewById(R.id.login);
        errorMessage = (TextView) findViewById(R.id.errorMessage);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        String newInstall = mSharedPreferences.getString(PREF_NEWINSTALL, "");
        final String versionName = BuildConfig.VERSION_NAME;

        if (!newInstall.equals(versionName)) {

            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(PREF_NEWINSTALL, versionName);
            e.commit();

            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);

            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.whatsnew_popup, null);

            builder.setView(dialogView);

            final TextView popup_text = (TextView) dialogView.findViewById(R.id.text_pop);
            final Button closeBtn = (Button) dialogView.findViewById(R.id.button_popup_close);

            final AlertDialog dialog = builder.create();

            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();


        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HttpsTrustManager.allowAllSSL();
                StringRequest request = new StringRequest(Request.Method.POST, loginUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsnobject = new JSONObject(response);
                            System.out.println("Connected");
                            JSONArray jsonArray = jsnobject.getJSONArray("statusMessage");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject statusObject = jsonArray.getJSONObject(i);

                                String statusMessage = statusObject.getString("resultCode");
                                String pnr = statusObject.getString("pnummer");
                                String functie = statusObject.getString("functie");
                                String voornaam = statusObject.getString("voornaam");
                                String tussenvoegsel = statusObject.getString("tussenvoegsel");
                                String achternaam = statusObject.getString("achternaam");
                                String mentorenGroep = statusObject.getString("mentoren_groep");
                                String aantalGroepen = statusObject.getString("aantal_groepen");

                                System.out.println("mentorengroep: " + mentorenGroep);
                                System.out.println("aantal groepen: " + aantalGroepen);

                                if (statusMessage.equals("1")) {

                                    if (pnr.equals(passText.getText().toString())) {
                                        wijzigPassword(pnr, functie, voornaam, tussenvoegsel, achternaam);
                                    } else {

                                        SharedPreferences.Editor e = mSharedPreferences.edit();
                                        e.putString(PREF_ID, pnr);
                                        e.putString(PREF_FUNCTIE, functie);
                                        e.putString(PREF_VOORNAAM, voornaam);
                                        e.putString(PREF_TUSSENVOEGSEL, tussenvoegsel);
                                        e.putString(PREF_ACHTERNAAM, achternaam);
                                        e.putString(PREF_MENTORENGROEP, mentorenGroep);
                                        e.putString(PREF_AANTALGROEPEN, aantalGroepen);

                                        e.commit();

                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {
                                    //loginText.setText("");
                                    passText.setText("");
                                    errorMessage.setText("Uw inlog gegevens zijn incorrect!");
                                    System.out.println("verkeerde gegevens");

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> parameters = new HashMap<String, String>();
                        parameters.put("logintext", loginText.getText().toString());
                        parameters.put("passtext", passText.getText().toString());
                        return parameters;
                    }
                };
                requestQueue.add(request);
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {

            this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }

    public void wijzigPassword(String pnr, String functie, String voornaam, String tussenvoegsel, String achternaam) {
        Toast.makeText(LoginActivity.this, "U logged voor het eerst in en daarom moet het password worden gewijzigd!!!",
                Toast.LENGTH_LONG).show();

        mSharedPreferences = getSharedPreferences(PREFS, MODE_PRIVATE);
        SharedPreferences.Editor e = mSharedPreferences.edit();
        e.putString(PREF_ID, "");
        e.commit();

        Intent intent = new Intent(getApplicationContext(), ChangePassword.class);
        intent.putExtra("pnr", pnr);
        intent.putExtra("functie", functie);
        intent.putExtra("voornaam", voornaam);
        intent.putExtra("tussenvoegsel", tussenvoegsel);
        intent.putExtra("achternaam", achternaam);
        startActivity(intent);
        finish();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Login Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
